$(document).ready(function() {
	$(".report_download_link").click(function(event){
		event.preventDefault();
		if (validate_data()){
			send_email_invoice();
		}
	});
	$("input[name='firstname']").bind("input change", function (){
		$(this).removeClass("error");
	});
	$("input[name='lastname']").bind("input change", function (){
		$(this).removeClass("error");
	});
	$("input[name='email']").bind("input change", function (){
		$(this).removeClass("error");
	});
	$("input[name='password']").bind("input change", function (){
		$(this).removeClass("error");
	});
	
});

function validate_data(){
	var firstname = $("input[name='firstname']");
	var lastname = $("input[name='lastname']");
	var email = $("input[name='email']");
	var password = $("input[name='password']");
	var data_is_valid = true;
	if (value_is_empty(firstname.val())){
		firstname.addClass("error");
		data_is_valid = false;
	}
	else{
		firstname.removeClass("error");
	}
	
	if (value_is_empty(lastname.val())){
		lastname.addClass("error");
		data_is_valid = false;
	}
	else{
		lastname.removeClass("error");
	}
	
	if (!validateEmail(email.val())){
		email.addClass("error");
		data_is_valid = false;
	}
	else{
		email.removeClass("error");
	}
	
	if (value_is_empty(password.val())){
		password.addClass("error");
		data_is_valid = false;
	}
	else{
		password.removeClass("error");
	}
	return data_is_valid;
}

function send_email_invoice(){
	var form = $("#invoice_details_form");
	$.post(form.attr("action"), form.serialize()).
		always(function (data){
			if (data=="true"){
				window.location.href = $(".report_download_link").attr("href");
			}
			else{
				alert(data);
			}
		});
}
