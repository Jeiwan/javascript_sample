$(document).ready(function() {
	
	if ($('#email').val()){
		display_login_form();
	}
	else{
		display_verifying_email();
	}
	
    /* email validation */
    var $email = $('#email');
    var $hint = $("#hint");
	$email.on('blur', function (){
		fnDisplayBtn();
	});
	
	$email.on('input', function (){
		fnDisplayBtn();
	});
	
    $email.on('keyup',function() {
    	fnDisplayBtn();
      	$hint.css('display', 'none').empty();
      	$(this).mailcheck({
        	suggested: function(element, suggestion) {
          	if(!$hint.html()) {
            	// First error - fill in/show entire hint element
            	var suggestion = "Did you mean <span class='suggestion'>" + "<span class='address'>" + suggestion.address + "</span>" + "@<a href='#' class='domain'>" + suggestion.domain + "</a></span>?";

            	$hint.html(suggestion).fadeIn(150);
          	} else {
            	// Subsequent errors
            	$(".address").html(suggestion.address);
            	$(".domain").html(suggestion.domain);
          	}
        	}
      	});
    });

    $hint.on('click', '.domain', function() {
      	// On click, fill in the field with the suggestion and remove the hint
      	$email.val($(".suggestion").text());
      	$hint.fadeOut(200, function() {
        	$(this).empty();
        	fnDisplayBtn();
      	});
      	return false;
    });
	/* end email validation */
    
    $('#confirm_email_btn').click(function(event) {
		var email_form = $('#booking_email_form_hidden');
		email_form.append($('input[name="service_type"]'));
		email_form.append($('#property_size'));
		email_form.append($('.total_cost'));
		$('#booking_email_form').submit();
	});
    /*
	$("#forgot_password").click(function(){
		$("#password_field").hide();
		$(this).hide();
		$("#login_btn").replaceWith("<a id='reset_pass_btn' href='#_' class='btn btn-default pull-right'>Reset password</a>");
		$("#reset_pass_btn").click(function(event) {
			event.preventDefault();
			window.location.assign("/?r=EyeonUnified\\EyeonClient\\EyeonResetPassword\\EyeonResetPassword");
		});
	});
	*/
	$('#login_btn').click(function(event) {
		event.preventDefault();
		var pass = $('#password').val();
		var email = $('#email').val();
		if(email && validateEmail(email) && pass && fnValidatePassword(pass)) {
			var email_form = $('#booking_email_form_hidden');
			email_form.append($('input[name="service_type"]'));
			email_form.append($('#property_size'));
			email_form.append($('.total_cost'));
			$('#booking_email_form').submit();
		}
	});
});

var email_timer = null;
var old_email = null;

function display_verifying_email(){
	$('#enter_password').hide();
	$('#confirm_email').show();
	$('#confirm_email_btn').html('Verifying email address ... ');
	$('#confirm_email_btn').attr('disabled', 'disabled');
}

function display_use_email(){
	var email = $('input[name="email_address"]').val();
	$('#enter_password').hide();
	$('#confirm_email').show();
	$('#confirm_email_btn').html('Click to use email '+email);
	$('#confirm_email_btn').removeAttr('disabled');
}

function display_login_form(){
	$('#confirm_email').hide();
	$('#enter_password').show();
}

function ajax_email_verify(email){
	$('#email_verify').val(email);
	var form = $('#booking_email_verify');
	$.post(form.attr('action'), form.serialize())
		.always(function (data){
			if (data==='true' && email===$('input[name="email_address"]').val()){
				display_use_email();
			}
			else if(data==='false' && email===$('input[name="email_address"]').val()){
				display_login_form();
			}
			else{
				display_verifying_email();		
			}
		});
}

function fnDisplayBtn(){
	var email = $('#email').val();
	if (old_email == email){
		return;
	}
	
	old_email = email;
	window.clearTimeout(email_timer);
	
	display_verifying_email();
	
	if (!validateEmail(email)){
		return;
	}
	if(window.location.href.substr(0,4) == "file"){
		if (email=="admin@user.com" || email=="benhammouda.chaker@gmail.com"){
			display_login_form();
		}
		else{
			display_user_email();
		}
	}
	else{
		email_timer = window.setTimeout(function (){
				ajax_email_verify(email);
			}, 1000);
	}
}

function fnValidatePassword(password) {
	if(password == undefined || password == '')
		return false;
	else
		return true;
}

