$(document).ready(function() {

	var validateEmail = function(email) {
		var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
		return re.test(email);
	};

	$("#forgot_password").click(function(){
		$("#password_field").hide();
		$(this).hide();
		$("#login_btn").replaceWith("<a id='reset_pass_btn' href='#_' class='btn btn-default pull-right'>Reset password</a>");
		$("#reset_pass_btn").click(function(event) {
			event.preventDefault();
			window.location.assign("/?r=EyeonUnified\\EyeonClient\\EyeonResetPassword\\EyeonResetPassword");
		});
	});

	$('#login_btn').click(function(event) {
		event.preventDefault();
		var pass = $('#password').val();
		var email = $('#email').val();
		if(email && validateEmail(email) && pass && fnValidatePassword(pass)) {
			var email_form = $('#booking_email_form_hidden');
			email_form.append($('input[name="service_type"]'));
			email_form.append($('#property_size'));
			email_form.append($('.total_cost'));
			$('#booking_email_form').submit();
		}
	});
});

function fnValidatePassword(password) {
	if(password == undefined || password == '')
		return false;
	else
		return true;
}
