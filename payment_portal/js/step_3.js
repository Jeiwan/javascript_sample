$(document).ready(function() {

	$('#create_booking_btn').click(function(event) {
		event.preventDefault();
		if (validate_input(false)){
			update_modal();
			$('#step_3').modal('show');
			$(".modal-footer a[href='05_confirmation.html']").bind('click', process_payment);
			//$('#eyeon_t_n_c').change();
		}
	});
	
	
	$('input[name="card_number"],input[name="card_expiry"],input[name="card_name"]').change(function(){
		$('input[name="bank_token"]').val("");
	});
	
	$('input[name="card_number"],input[name="card_expiry"],input[name="card_name"]').keyup(function(){
		$('input[name="bank_token"]').val("");
	});
	
	$('input[name="card_name"],input[name="card_number"],input[name="card_expiry"],input[name="card_cvv"]').keyup(function(){
		validate_input(true);
		$(this).removeClass('invalid');
	});
	
	$('input[name="card_name"],input[name="card_number"],input[name="card_expiry"],input[name="card_cvv"]').focus(function(){
		validate_input(true);
	});
	
	$('input[name="card_name"],input[name="card_number"],input[name="card_expiry"],input[name="card_cvv"]').blur(function(event) {
		return validate_input(true);
	});
	
	$('#eyeon_t_n_c, #buyer_fee').change(function(event) {
		if($('#eyeon_t_n_c').is(':checked') && $('#buyer_fee').is(':checked')) {
			$('.modal-footer a[href="05_confirmation.html"]').show();
		}
		else{
			$('.modal-footer a[href="05_confirmation.html"]').hide();
		}
	});
	
	$("#step_3 a[data-dismiss='modal']").click(function (event){
		
	});
	
	validate_input(true);
});
var debug=0;

function update_modal(){
	update_modal_card_info();
	//update_modal_booking_info();
	update_modal_payment_status();
}

function update_modal_booking_info(){
	var property_size = $('#property_size').find(':selected');
	$('span.modal_property_size').text(property_size.text());
	$('span.modal_total').text('$'+property_size.attr('data-cost'));
	
	var service_type = $('input[name="service_type"]:checked');
	
	if(service_type.val() == 1) {
		$('.modal-body p strong').text('Building & Pest Report');
		$('span.modal_property_size').text($('#property_size :selected').text());
		$("#step_3 a[href='05_confirmation.html']").text("Pay $"+property_size.attr('data-cost')+" and Book Inspection Time");
	} else if(service_type.val() == 2) {
		$('.modal-body p strong').text('Strata Report');
		$('span.modal_property_size').text("Strata");
		$("#step_3 a[href='05_confirmation.html']").text("Pay $"+service_type.attr('data-cost')+" and Enter Property Details");
	
	}
}
function update_modal_card_info(){
	var card_number = $('input[name="card_number"]').val();

	var visa = 4;
	var mastercard = 5;
	
	if(card_number[0] == visa) {
		$('.modal_card_issuer').text('VISA')
	} else if(card_number[0] == mastercard) {
		$('.modal_card_issuer').text('Mastercard')
	}
	
	var last3 = card_number.substr(card_number.length-3);
	$('.modal_card_number').text(last3);
	$("input[name='last_digits']").val(last3);
	
	$('span.modal_card_expiry').text($('input[name="card_expiry"]').val());
}

function update_modal_payment_status(){
	$("#step_3 a[href='05_confirmation.html']").unbind('click', process_payment);
	$("#step_3 a[href='05_confirmation.html']").unbind('click', submit_payment_form);
	$("#step_3 a[data-dismiss='modal']").show();
	
	$("#txtStatus").remove();
	
	$("#eyeon_t_n_c").attr('checked', false);
	$("#buyer_fee").attr('checked', false);
	$("#step_3 a[href='05_confirmation.html']").hide();
	$("#step_3 a[href='05_confirmation.html']").removeAttr('disabled');
	$("#step_3 a[href='05_confirmation.html']").text("Process Payment");
}

function validate_input(accept_empty){
	var data_is_valid = true;
	
	var cvv_is_valid = true;
	var number_is_valid = true;
	var expiry_is_valid = true;
	var name_is_valid = true;
	
	var cvv_is_empty = false;
	var number_is_empty = false;
	var expiry_is_empty = false;
	var name_is_empty = false;
	
	if ($("input[name='bank_token']").val() != ""){
		var cvv = $('input[name="card_cvv"]').val();
		cvv_is_empty = cvv == undefined || cvv == '';
		cvv_is_valid = !cvv_is_empty && cvv.length == 3;
	}
	else{
		$('[data-numeric]').payment('restrictNumeric');
	  	$('input[name="card_number"]').payment('formatCardNumber');
	  	$('input[name="card_expiry"]').payment('formatCardExpiry');
	  	$('input[name="card_cvv"]').payment('formatCardCVC');
	  	
	  	$('input').removeClass('invalid');
	    $('.validation').removeClass('passed failed');

		// Validate Card Number
		var cardNo = $('input[name="card_number"]').val();
		number_is_empty = cardNo == undefined || cardNo == '';
		number_is_valid = $.payment.validateCardNumber(cardNo);
		
		// Validate CVV
		var cvv = $('input[name="card_cvv"]').val();
		var cardType = $.payment.cardType(cardNo);
		cvv_is_empty = cvv == undefined || cvv == '';
		cvv_is_valid = $.payment.validateCardCVC(cvv, cardType);
		
		// Validate Expiry
		var expiry = $('input[name="card_expiry"]').val();
		expiry_is_empty = expiry == undefined || expiry == '';
		expiry_is_valid = $.payment.validateCardExpiry($('input[name="card_expiry"]').payment('cardExpiryVal'));
		
		// Validate Name
		var name = $('input[name="card_name"]').val();
		name_is_empty = name == undefined || name == '';
		name_is_valid = !name_is_empty;
	}
	
	$('input[name="card_number"]').toggleClass('invalid', !(number_is_valid || accept_empty && number_is_empty));
	$('input[name="card_expiry"]').toggleClass('invalid', !(expiry_is_valid || accept_empty && expiry_is_empty));
	$('input[name="card_cvv"]').toggleClass('invalid', !(cvv_is_valid || accept_empty && cvv_is_empty));
	$('input[name="card_name"]').toggleClass('invalid', !(name_is_valid || accept_empty && name_is_empty));

	data_is_valid = number_is_valid && cvv_is_valid && expiry_is_valid && name_is_valid;
	if (data_is_valid){
		$('#create_booking_btn').removeAttr('disabled');
	}
	else{
		$('#create_booking_btn').attr('disabled', 'disabled');
	}
	return data_is_valid;
}

textArea='';

function setStatus(status)
{
    if (!$('#txtStatus').length)
    {
        textArea = $('<textarea id="txtStatus" class="statusarea" cols=60 rows=5" />');
        $('#step_3 .modal-body').append(textArea);
        $(textArea).val(status + '\n');
    }
    else
    {
        t = textArea.val();
        textArea.val(t + status + '\n');
        var psconsole = textArea;
        psconsole.scrollTop(
            psconsole[0].scrollHeight - psconsole.height()
        );        
    }
} 

function process_payment(event)
{
	event.preventDefault();
    process_payment_loading();
    var bank_token = $("input[name='bank_token']").val();
    if (bank_token != ""){
    	request_trigger_payment(bank_token);
    }
    else{
		request_register_token();
    }
}

function request_register_token(){
	$.ajax({
		    url: "?r=EyeonUnified\\EyeonClient\\EyeonPaymentApi\\EyeonPaymentApi&query=/request/register_token/",
		    type: 'POST',
		    data: {
		        'card_number'	: $("input[name='card_number']").val(),
		        'card_expiry'	: $("input[name='card_expiry']").val(),
		        'card_cvv'		: $("input[name='card_cvv']").val(),
		        'card_name'		: $("input[name='card_name']").val()
		    },
		    success: function(data) {
		    		var request_token = "";
		    		try{
		    			var response = $.parseJSON(data);
						if (response.result){
							request_token = response.request_token;
		    			}
		    		}
		    		catch(e){
		    			request_token = "";
		    		}
		    		if (request_token != ""){
		    			setStatus('- Requested register bank token');
		    			response_token_register(request_token, 25);
		    		}
		    		else{
		    			process_payment_error('Received invalid request ID.');
		    		}
		        },
		    complete: function(){
		    },
		    error: function(data, errorThrown) {
		        process_payment_error(errorThrown);
		    }
		});
}

function response_token_register(request_token, attempts){
	if (attempts <= 0){
		process_payment_error('Timeout.');
		return;
	}
	$.ajax({
	    url: "?r=EyeonUnified\\EyeonClient\\EyeonPaymentApi\\EyeonPaymentApi&query=/response/register_token/",
	    type: 'POST',
	    data: {
	        'request_token'	: request_token
	    },
	    success: function(data) {
	    		try{
	    			var response = $.parseJSON(data);
	    			if (response.status == "done"){
	    				setStatus('- Register bank token - Done');
	    				var nab_response = $.parseJSON(response.data);
	    				setStatus('- ' + nab_response['text']);
						if (nab_response['code'] == 1)
						{
							request_trigger_payment(nab_response.bank_token);
						}
						else{
							process_payment_error(nab_response['nab_text']);
						}
					}
					else if (response.status == "waiting"){
						setStatus('- Register bank token - Waiting');
						setTimeout(function(){
							response_token_register(request_token, attempts-1)
						}, 1000);
					}
					else{
						setStatus('- Register bank token - Error');
						process_payment_error('Failed to get bank token.');
					}
	    		}
	    		catch(e){
	    			setStatus('- Register bank token - Exception');
	    			process_payment_error('Failed to get bank token. - '+e.message);
	    		}
	        },
	    complete: function(){
	    },
	    error: function(data, errorThrown) {
	    	setStatus('- Register bank token - Fatal Error');
	        process_payment_error(errorThrown);
	    }
	});
}

function request_trigger_payment(bank_token){
	$("input[name='bank_token']").val(bank_token);	
	$.ajax({
	    url: "?r=EyeonUnified\\EyeonClient\\EyeonPaymentApi\\EyeonPaymentApi&query=/request/trigger_payment/",
	    type: 'POST',
	    data: {
	        'bank_token'	: bank_token,
	        'card_cvv'		: $("input[name='card_cvv']").val(),
	        'payment_amount': $('.report_price:first').text().replace('$','')
	    },
	    success: function(data) {
	    		var request_token = "";
	    		try{
	    			var response = $.parseJSON(data);
					if (response.result){
						request_token = response.request_token;
	    			}
	    		}
	    		catch(e){
	    			request_token = "";
	    		}
	    		if (request_token != ""){
	    			setStatus('- Trigger payment token.');
	    			response_trigger_payment(request_token, 25);
	    		}
	    		else{
	    			process_payment_error('Received invalid request ID.');
	    		}
	        },
	    complete: function(){
	    },
	    error: function(data, errorThrown) {
	        process_payment_error(errorThrown);
	    }
	});
}

function response_trigger_payment(request_token, attempts){
	if (attempts <= 0){
		process_payment_error('Timeout.');
		return;
	}
	$.ajax({
	    url: "?r=EyeonUnified\\EyeonClient\\EyeonPaymentApi\\EyeonPaymentApi&query=/response/trigger_payment/",
	    type: 'POST',
	    data: {
	        'request_token'	: request_token
	    },
	    success: function(data) {
	    		var bank_token = "";
	    		try{
	    			var response = $.parseJSON(data);
	    			if (response.status == "done"){
	    				setStatus('- Trigger payment - Done');
	    				var nab_response = $.parseJSON(response.data);
	    				setStatus('- ' + nab_response['text']);
						if (nab_response['code'] == 3)
						{
							process_payment_success();
						}
						else{
							process_payment_error(nab_response['nab_text']);
						}
					}
					else if (response.status == "waiting"){
						setStatus('- Trigger payment - Waiting');
						setTimeout(function(){
							response_trigger_payment(request_token, attempts-1)
						}, 1000);
					}
					else{
						setStatus('- Trigger payment - Error');
						process_payment_error('Failed to trigger payment.');
					}
	    		}
	    		catch(e){
	    			setStatus('- Trigger payment - Exception');
	    			process_payment_error('Failed to trigger payment - '+e.message);
	    		}
	        },
	    complete: function(){
	    },
	    error: function(data, errorThrown) {
	    	setStatus('- Trigger payment - Fatal Error');
	        process_payment_error(errorThrown);
	    }
	});
}

function process_payment_loading(){
	$("#step_3 a[href='05_confirmation.html']").unbind('click', process_payment);
	$("#step_3 a[data-dismiss='modal']").hide();
	$("#step_3 a[href='05_confirmation.html']").attr('disabled', 'disabled');
	$("#step_3 a[href='05_confirmation.html']").text('Processing...');
}

function process_payment_success(){
	var button_text = 'Continue';
	/*
	if ($("#inspection_type_2").is(':checked')){
		button_text = 'Next step: Enter Property Details';
	}
	*/
	$("#step_3 a[href='05_confirmation.html']").text(button_text);
	$("#step_3 a[href='05_confirmation.html']").removeAttr('disabled');
	$("#step_3 a[href='05_confirmation.html']").bind('click', submit_payment_form);
}

function process_payment_error(message){
	setStatus('- Error: ' + message);
    $("#step_3 a[href='05_confirmation.html']").text('Error');
    $("#step_3 a[data-dismiss='modal']").show();
}

function submit_payment_form(event){
	event.preventDefault();
	var newForm = $('#credit_card_details_form');
	newForm.append($('input[name="service_type"]').clone().attr('style', 'display: none'));
	newForm.append($('#property_size').clone().attr('style', 'display: none'));
	newForm.submit();
}

