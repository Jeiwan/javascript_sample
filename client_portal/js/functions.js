$(document).ready(function() {

	/* step 1 */

	$('#inspection_type_1').click(function() {
   		if($('#inspection_type_1').is(':checked')) {
   			$(".next_step").text("Next: Book inspection time");
			$("#property_size_container").show();
			
			if($( "#property_size option:selected" ).val() == 0) {
			    fnChangeBookNowBtn();
			}
			$("#property_size").change();
			$("p.error_report").hide();
		}
	});
	$('#inspection_type_2').click(function() {
		$(".next_step").text("Next: Enter property details");
   		if($('#inspection_type_2').is(':checked')) {
			$("#property_size_container").hide();
			$("#total_field").show();
			$("#book_now_btn").replaceWith( $("#book_now_btn").clone(true).text("Book now"));
			$(".total_cost").val('$' + $("#inspection_type_2").attr('data-cost'));
		}
		$("p.error_report").hide();
		fnRestoreBookNowBtn();
	});

	$("#property_size").change(function(){
		var selectedValue = $(this).find(":selected").val();
		//$("#inspection_type_1").val(selectedValue);
		var total = parseInt($("#inspection_type_1").val()) + parseInt($("#inspection_type_2").val());
		$(".total_cost").val('$' + $("#property_size option:selected").attr('data-cost'));
		if (this.value == '0') {
		    fnChangeBookNowBtn();
		    $("#total_field").hide();
		} else {
			fnRestoreBookNowBtn();
			$("#total_field").show();
		}
	});
	//$("#property_size").change();
	$("#contact_eyeon_txt").hide();
	$("input[name='service_type']:checked").click();
	
	/* end step 1 */
	$('.tooltip_wrapper').tooltip();
	
	/* add code to submit form on click */
	$('#book_now_btn').click(function(event) {
		event.stopPropagation();
		var submit_url = $('form').attr('action');
		$('form').submit();
	});

	

	fnShowPropertySizes();
	
	
	$('.logout_btn').click(function (event){
		event.preventDefault();
		$.get("?r=EyeonUnified\\EyeonClient\\EyeonConfirmation\\EyeonConfirmationAction&action=logout")
			.always(function(data){
					window.location.href = "http://www.eyeon.com.au";
					});
	});

});

function disableLink(e) {
    e.preventDefault();

    return false;
}

$.fn.disable = function() {
    return this.each(function() {
    	if ($(this).prop("tagName")=='A' || $(this).prop("tagName")=='a'){
        	$(this).bind('click', disableLink);
        }
        else if (typeof this.disabled != "undefined"){
        	this.disabled = true;
        }
    });
}

$.fn.enable = function() {
    return this.each(function() {
    	if ($(this).prop("tagName")=='A' || $(this).prop("tagName")=='a'){
    		$(this).unbind('click', disableLink);
    	}
        else if (typeof this.disabled != "undefined"){
        	this.disabled = false;
        }
    });
}



function fnShowPropertySizes() {
	if($('#inspection_type_1').is(':checked')) {
		$("#property_size_container").show();
	}
}

function fnRestoreBookNowBtn() {
	$("#widget_call_btn").hide();
	$(".step-2-main *").enable();
	$(".step-3-main *").enable();
	$("#book_now_btn").show();
    $("#contact_eyeon_txt").hide();
	/*
    var bookNowBtn = $("#book_now_btn_inactive");
    if((bookNowBtn == undefined) || (bookNowBtn == null) || (bookNowBtn == '')) {
	    bookNowBtn = $("#book_now_btn");
    }
    bookNowBtn.replaceWith( bookNowBtn.clone(false).text("Book Now").attr('id','book_now_btn'));
    $("#book_now_btn").click(function(event) {
	    event.stopPropagation();
	    var submit_url = $('form').attr('action');
	    $('form').submit();
    });
    $("#book_now_btn").attr('href', '#');
    */
}

function fnChangeBookNowBtn() {
	$("#widget_call_btn").show();
	$(".step-2-main *").disable();
	$(".step-3-main *").disable();
	$("#book_now_btn").hide();
    $("#contact_eyeon_txt").show();
	/*
    var bookNowBtn = $("#book_now_btn");
    bookNowBtn.replaceWith(bookNowBtn.clone(false).text("Please call 1300 555 666 to arrange your inspection").attr('id', 'book_now_btn_inactive'));
    $("#book_now_btn_inactive").attr('href', 'tel:1300555666');
    */
}
