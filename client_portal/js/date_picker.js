$(document).ready(function() {
	$(".dp").each(function (){
		$(this).val($(this).attr('value'));
	});
	$(".dp").keypress(function( event ) {
		event.preventDefault();
	});
	$(".dp").datepicker({
		dateFormat:"d M, yy",
		onSelect: function(dateText) {
			$(this).val(dateText);
			$(this).blur();
		}	
	});
});
