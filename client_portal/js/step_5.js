$(document).ready(function() {

	/* calendar - this may be removed completely depending on what the developer will do for step 5 of the process ( eg: if the decides to use a different plugin or a custom solution depending on how it needs to interact with the backend/db ) */

	/*var date = new Date();
	var d = date.getDate();
	var m = date.getMonth();
	var y = date.getFullYear();*/
	
	function process_events(slotsStr){
		if (slotsStr.length>0 && slotsStr.charAt(0) == '"'){
			slotsStr = slotsStr.substr(1, slotsStr.length-2);
		}
		var selectedSlots = slotsStr.split(';');
		var events = [];
		$('.fc-day').each(function() {
			var today = new Date();
			var day = $.fullCalendar.parseDate($(this).attr('data-date'));
			var daysEvents = [];
			if (day.getDay() == 0 || day.getDay() == 6){
				fnStrikeOffDay($(this).attr('data-date'));
				return;
			}
			if(today.getTime() < day.getTime()) {
				var startHr = 9;
				var startMin = 0;
				var multiple = false;
				
				for(var i = 0; i < selectedSlots.length; i++) {
					var slotTimes = selectedSlots[i].split(" to ");
					if(slotTimes.length <= 1) {
						break;
					}
					var slotStart = $.fullCalendar.parseDate(slotTimes[0]);
					var slotEnd = $.fullCalendar.parseDate(slotTimes[1]);
					var y = day.getFullYear();
					var m = day.getMonth();
					var d = day.getDate();

					if(
						y == slotStart.getFullYear() &&
						m == slotStart.getMonth() &&
						d == slotStart.getDate()
					) {
							multiple = true;
							if( startHr == slotStart.getHours() && startMin == slotStart.getMinutes() ) {
								startHr = slotEnd.getHours();
								startMin = slotEnd.getMinutes();
								continue;
							}
							
							var startTime = new Date(y, m, d, startHr, startMin, 0);
							var endTime = new Date(y, m, d, slotStart.getHours(), slotStart.getMinutes(), 0);
							// Correct bug where there's time overlap - temporary fix for testing
							if(startTime > endTime) {
								continue;
							}
							// END: Correct bug where there's time overlap - temporary fix for testing
							var newEvent = {
								//id: "DAY_"+y+"_"+m+"_"+d,
								title: ""+startHr+":"+(startMin < 10 ? "0"+startMin : startMin)+" - "+slotStart.getHours()+":"+(slotStart.getMinutes() < 10 ? "0"+slotStart.getMinutes() : slotStart.getMinutes()),
								start: startTime,
								end: endTime
							};
							daysEvents.push(newEvent);
							startHr = slotEnd.getHours();
							startMin = slotEnd.getMinutes();
					}
				}
				/* TODO:	Check the array;
				 * 			if it has more than 3, truncate it to 3,
				 *	 		push the 3 to the events array.
				 * 			display number of remaining selections
				 **/


				if(multiple) {
					if(startHr < 17) {
						var startTime = new Date(y, m, d, startHr, startMin, 0);
						var newEvent = {
							//id: "DAY_"+day.getFullYear()+"_"+day.getMonth()+"_"+day.getDate(),
							title: ""+startHr+":"+(startMin < 10 ? "0"+startMin : startMin)+" - "+17+":00",
							start: startTime,
							end: new Date(day.getFullYear(), day.getMonth(), day.getDate(), 17, 0, 0)
						};
						daysEvents.push(newEvent);
					}
				} else {
					var newEvent = {
						//id: "DAY_"+day.getFullYear()+"_"+day.getMonth()+"_"+day.getDate(),
						title: " 9:00 - 17:00",
						start: new Date(day.getFullYear(), day.getMonth(), day.getDate(), 9, 0, 0),
						end: new Date(day.getFullYear(), day.getMonth(), day.getDate(), 17, 0, 0)
					};
					daysEvents.push(newEvent);
				}
			}

			if(daysEvents.length == 0) {
				fnStrikeOffDay(day);
			} else {
				var daysLeft = daysEvents.length - 2;
				for( var i = 0; i<daysEvents.length; i++) {
					if (i < 2){
						daysEvents[i].visible = true;
					}
					else if (i==2){
						daysEvents[i].visible = true;
						daysEvents[i].title = "+ "+daysLeft+" more";
					}
					else{
						daysEvents[i].visible = false;
					}
					events.push(daysEvents[i]);
				}
			}
		});
		
		return events;
		
	}
	
	function wait4reserve(api_url, query, attempts){
		if (attempts < 1){
			$("#reserving_time").modal('hide');
			$("#error_reserving_time").modal('show');
		}
		var timestamp = new Date();
		$.get( api_url, { query: query, timestamp: timestamp.getTime()}).always(function(data){
			try{
				var response = $.parseJSON(data);
				if (response.status == "done" && response.data=="true"){
			  		$("#reserving_time").modal('hide');
					$('form').submit();
				}
				else if (response.status == "waiting"){
					setTimeout(function(){
						wait4reserve(api_url, query, attempts-1);
					}, 5000);
				}
				else{
					$("#reserving_time").modal('hide');
					$("#error_reserving_time").modal('show');
				}
			}
			catch (err){
				$("#reserving_timer").modal('hide');
				$("#error_reserving_time").modal('show');
			}
		});
	}
	
	function wait4available(api_url, query, callback, attempts){
		if (attempts < 1){
			$("#loading_calendar").modal('hide');
			$("#error_loading_calendar").modal('show');
		}
		var timestamp = new Date();
		$.get( api_url, { query: query, timestamp: timestamp.getTime()}).always(function(data){
			try{
				var response = $.parseJSON(data);
				if (response.status == "done"){
					var events = process_events(response.data);
			  		$("#loading_calendar").modal('hide');
					callback(events);
				}
				else if (response.status == "waiting"){
					setTimeout(function(){
						wait4available(api_url, query, callback, attempts-1);
					}, 5000);
				}
				else{
					$("#loading_calendar").modal('hide');
					$("#error_loading_calendar").modal('show');
				}
			}
			catch (err){
				$("#loading_calendar").modal('hide');
				$("#error_loading_calendar").modal('show');
			}
		});
	}

	var calendar = $('#calendar').fullCalendar({
		header: {
			left: 'prev',
			center: 'title',
			right: 'next'
		},
		timezone: "Australia/Sydney",

		titleFormat: {
        	day: 'MMM d, yyyy'
    	},

    	eventClick: function(event) {
			$('#step_5').modal('show');
			var new_date = $.fullCalendar.formatDate(event.start, "d MMM, yyyy");
			$('span.modal_date').text(new_date);
			
			var timeSlots = fnGetAvailableTimeSlots($('#calendar'), event.start);

			// Get modal dialog radiobox - clone it
			var template = $('form .radio-inline:first').clone(true);
			$('form label.radio-inline').remove();

			fnSetupTimeSelectionModal(timeSlots, template);
		},

		dayClick: function(date) {
			var timeSlots = fnGetAvailableTimeSlots($('#calendar'), date);
			if (timeSlots.length == 0)
				return;
				
			$('#step_5').modal('show');
			var new_date = $.fullCalendar.formatDate(date, "MMM d, yyyy");
			$('span.modal_date').text(new_date);

			/* Get modal dialog radiobox - clone it */
			var template = $('form .radio-inline:first').clone(true);
			$('form label.radio-inline').remove();
			
			fnSetupTimeSelectionModal(timeSlots, template);
		},

		events: function(start, end, callback) {
			/**
			 * This function creates the time slots that are available
			 * and displays them on the calendar
			 **/
			$("#loading_calendar").modal('show');
			if(window.location.href.substr(0,4) == "file"){
				
				setTimeout(function (){
					$("#loading_calendar").modal('hide');
					events = process_events("");
					callback(events);
				}, 5000);
				return;
			}
			var startDate = start.toISOString().substr(0,10);
			var endDate = end.toISOString().substr(0,10);
			var data = "{\"startDate\":\""+startDate+"\", \"endDate\":\""+endDate+"\"}";
			var timestamp = new Date();
			$.get( api_url, { query: request_available_query, data: data, timestamp: timestamp.getTime()}).always(function(data) {
				var result = false;
				try{
					var response = $.parseJSON(data);
					result = response.result;
				}
				catch(err){
					result = false;
				}
				if (result){
					var query = response_available_query;
					setTimeout(function (){
						wait4available(api_url, response_available_query, callback, 10);
					}, 5000);					
				}
				else{
					$("#loading_calendar").modal('hide');
					$("#error_loading_calendar").modal('show');
				}
				
			});
		},
		
		dayRender: function(date, cell) {
			var today = new Date();
			var cellDate = $.fullCalendar.parseDate(date);
			if(today.getTime() > cellDate.getTime()) {
				fnStrikeOffDay(date);
			}
			//$('.fc-today').removeClass('striken_off');
		},
		
		eventRender: function(event, element) {
			if (event.visible){
				return element;
			}
			else{
				return false;
			}
   		}
	});
	/* end calendar =============== */

	/* step 5 temporary behavior - depends on how the calendar will actually be implemented with the backend - again, this may be removed completely depending on that

	var modal_hour = $('input[name="time_selection"]:checked').val();
	$("span.modal_hour").text(modal_hour);

	$('input[name="time_selection"]').change(function()
	{
		var modal_hour = $('input[name="time_selection"]:checked').val();
		$("span.modal_hour").text(modal_hour);
	});
	// end step 5 behavior */
	

function skip_calendar(){
	$("input[name='time_selection']:last").prop('checked', true);
	if(window.location.href.substr(0,4) == "file"){
		window.location.href="step_6.html";
	}
	else{
		$("form").submit();
	}
}
$(".skip_calendar_btn").click(function(event){
	event.preventDefault();
	skip_calendar();
});
$('a[href="step_6.html"]').click(function(event) {
		event.preventDefault();
		var start = $("input:checked[name=time_selection]").val();
		
		if (typeof(start) === "undefined") {
			return;
		}
		$("#step_5").modal('hide');
		$("#reserving_time").modal('show');
		if(window.location.href.substr(0,4) == "file"){
			setTimeout(function (){
				$("#reserving_time").modal('hide');
				window.location.href="step_6.html";
			}, 5000);
			return;
		}
		
		var data = "{\"start\":\""+start+"+10:00\"}";
		var timestamp = new Date();
		$.get( api_url, { query: request_reserve_query, data: data, timestamp: timestamp.getTime()}).always(function(data) {
			var result = false;
			try{
				var response = $.parseJSON(data);
				result = response.result;
			}
			catch(err){
				result = false;
			}
			if (result){
				setTimeout(function (){
					wait4reserve(api_url, response_reserve_query, 10);
				}, 5000);					
			}
			else{
				$("#reserving_time").modal('hide');
				$("#error_reserving_time").modal('show');
			}
			
		});
		
	});

});

function fnCreateTimeRadios(start, end, template) {
	var j = 0;

	for(;start.getTime() < end.getTime();) {
		var hr = start.getHours();
		var min = (start.getMinutes() < 10 ? "0"+start.getMinutes() : start.getMinutes());

		var startPlusDiff = new Date(start.getTime() + (30*60*1000));
		
		var proto = template.clone(true);
		var dateStr = $.fullCalendar.formatDate(start, 'yyyy-MM-dd');
		proto.find('input[name="time_selection"]').attr('id', 'time_selection_'+(++j));
		proto.find('input[name="time_selection"]').attr('value', dateStr+" "+hr+":"+min+":00");
		proto.find('input[name="time_selection"]').prop('checked', false);
		temp = proto.find('input[name="time_selection"]').clone();
		var h = hr % 12;
		if (h === 0) h = 12;
		var hr_str = h + ":"+min + (hr < 12 ? 'am' : 'pm');
		proto.html(hr_str);
		proto.append(temp);
		$('form').append(proto);
		start = new Date(start.getTime() + (30*60*1000));
	}

	fnSetupEventHandlers();
}

function fnSetupEventHandlers() {
	$('input[name="time_selection"]').change(function() {
		var selected = $('input[name="time_selection"]:checked').val();
		var time = selected.substr(11,5);
		var hr_min = time.split(":");
		var append = '';

		if(hr_min[0] >= 12) {
			append = "PM";
			if(hr_min[0] > 12) {
				hr_min[0] -= 12;
			}
		} else {
			append = "AM";
		}

		$('span.modal_hour').text(hr_min[0]+":"+hr_min[1]+append);
	});
}

function fnGetAvailableTimeSlots(calendar, date) {
	// Get available calendar time slots
	var timeSlots = calendar.fullCalendar('clientEvents', function(timeSlot) {
		if(timeSlot.start.getFullYear() == date.getFullYear() &&
			timeSlot.start.getMonth() == date.getMonth() &&
			timeSlot.start.getDate() == date.getDate()
		) {
			return true;
		} else {
			return false;
		}
	});

	if(timeSlots.length == 0) {
		fnStrikeOffDay(date);
	}

	return timeSlots;
}

function fnSetupTimeSelectionModal(timeSlots, template) {
	for( var i = 0; i < timeSlots.length; i++ ) {
		var slot = timeSlots[i];
		var start = slot.start;
		var end = slot.end;
		fnCreateTimeRadios(start, end, template);
	}
	proto = template.clone(true);
	proto.attr("style", "display:none");
	fnCreateTimeRadios(new Date(0), new Date(10), proto);
}

function fnStrikeOffDay(date) {
	var cellDate = $.fullCalendar.parseDate(date);
	var dateStr = cellDate.getFullYear()
		+"-"
		+(cellDate.getMonth()>=9?(cellDate.getMonth()+1):"0"+(cellDate.getMonth()+1))
		+"-"
		+(cellDate.getDate()>=10?cellDate.getDate():"0"+cellDate.getDate());

	$('.fc-day[data-date="' + dateStr + '"]').addClass('striken_off');
}


function fnCreateEvent(startHr, startMin, startTime, endTime) {
	return {
		//id: "DAY_"+day.getFullYear()+"_"+day.getMonth()+"_"+day.getDate(),
		title: ""+startHr+":"+(startMin < 10 ? "0"+startMin : startMin)+" - "+17+":00",
		start: startTime,
		end: endTime
	};
}
