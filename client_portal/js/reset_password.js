$(document).ready(function() {

	$('#reset_password_activate a').click(function(event) {
		event.preventDefault();

		$('#password').removeClass('error');
		$('#password_confirm').removeClass('error');
		if(fnCheckValid()) {
			$('#reset_password_form').submit();
		} else {
			$('.error_report').show();
			$('#password').addClass('error');
			$('#password_confirm').addClass('error');
		}
	});

	$('#password, #password_confirm').keyup(function(event) {
		$(this).removeClass('error');
	});

});

function fnCheckValid() {
	var pass = $('#password').val();
	var pass2 = $('#password_confirm').val();
	if(pass == '' || pass == undefined) {
		return false;
	}
	if(pass === pass2) {
		return true;
	} else {
		return false;
	}
	pass = null;
	pass2 = null;
}