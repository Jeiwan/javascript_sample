$(document).ready(function() {

	$('a[href="step_5.html"]').click(function(event) {
		event.preventDefault();
		$('form').submit();
	});

	$('input[name="postcode"]').keydown(function(event) {
		var kc = 0;
		
		$(this).removeClass('error');
		if(window.event) {
			kc = event.keyCode;
		} else if(event.which) {
			kc = event.which;
		}

		if( (kc==8) || (kc==9) || (kc>=16 && kc<=18) || (kc == 32)
			|| (kc>=37 && kc<=40) || (kc>=48 && kc<=57)
			|| (kc>=96 && kc<=105) || (kc==173) || (kc==46) ) {
				var pc_val = $(this).val();
				if( ((kc>=48 && kc<=57) || (kc>=96 && kc<=105)) && (pc_val.length < 4)) {
					return true;
				} else if(!((kc>=48 && kc<=57) || (kc>=96 && kc<=105))){
					return true;
				} else {
					return false;
				}
		} else {
			return false;
		}
	});

});
