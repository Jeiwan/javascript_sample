$(document).ready(function() {
	$('.agent_details').hide();
	$('.vendor_details').hide();
	
	$(function() {
		$("#accordion").accordion({
			heightStyle: "content",
            autoHeight: false,
        	clearStyle: true,
        	event: "none" 
		});
		$("#accordion h3").click(function (event){
			var active_id = $( "#accordion" ).accordion( "option", "active" );
			var res = $(this).attr('id').split('-');
			var destination_id = parseInt(res[res.length -1]);
			change_tab(active_id, destination_id);
		});
	});
	
	$('.agent_details_container').find('[name]').blur(function(event) {
		var name = $(this).attr('name');
		$('.vendor_details_container input[name="'+name+'"]').val($(this).val());
	});
	
	$('.vendor_details_container').find('[name]').blur(function(event) {
		var name = $(this).attr('name');
		$('.agent_details_container input[name="'+name+'"]').val($(this).val());
	});
	
	$("[data-type]").blur(function(){
		validate_field($(this));
	});
	
	$("input[name='client_type']").click(function(){
		$("#client_type_div").removeClass('invalid');
	});
	
	$('.save-details-btn').click(function(event) {
		event.preventDefault();
		if (validate_all_fields()){
			submit_form();
		}
	});
	
	$('#next_btn').click(function(event) {
		event.preventDefault();
		var destination_id = 0;
		if($('#client_type_1').is(':checked')) {
			destination_id = 1;
		} else {
			destination_id = 4;
		}
		change_tab(0, destination_id);
	});
	
	$('.nav-btn').click(function(event){
		event.preventDefault();
		var origin_id = parseInt($(this).attr('data-origine'));
		var destination_id = parseInt($(this).attr('data-destination'));
		change_tab(origin_id, destination_id);
	});
	
	
	function update_sale_date (){
		if ($(this).val() == 'Auction'){
			$("input[name='sale_date']").val("");
			$(".sale-div").show();	
		}
		else{
			$(".sale-div").hide();
			$("input[name='sale_date']").val("2099-01-01");
		}
	}
	
	$("select[name='sale_type']").click(update_sale_date);
	$("select[name='sale_type']").blur(update_sale_date);
	$("select[name='sale_type']").change(update_sale_date);
	$("select[name='sale_type']").keyup(update_sale_date);
	$("select[name='sale_type']").keypress(update_sale_date);
	
	$("select[name='sale_type']").click();
});

function value_is_empty(value){
	return value == undefined || value == null || value == '';
}

function validate_field(field){
	var is_valid = eval(field.attr('data-type')+"_is_valid(field)");
	if (is_valid){
		field.removeClass('invalid');
	}
	else{
		field.addClass('invalid');
	}
}

function validate_all_fields(){
	$('.invalid').removeClass('invalid');
	
	$('.property_details').next().find("[data-type]").each(function (){
		validate_field($(this));
	});
	
	var parentClass = '';
	if($('#client_type_1').is(':checked')) {
		parentClass = '.agent_details';
	} else if ($('#client_type_2').is(':checked')) {
		parentClass = '.vendor_details';
	}
	else{
		$("#client_type_div").addClass('invalid');
	}
	
	if ($(".invalid").length){
		return false;
	}
	
	$(parentClass).next().find("[data-type]").each(function (){
		validate_field($(this));
	});
	
	if ($(".invalid").length){
		return false;
	}
	
	return true;
}

function email_is_valid(email){
	if (email.attr("data-compulsory")=="no" && value_is_empty(email.val())){
		return true;
	}
	else{
		var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
		return re.test(email.val());
	}
}

function numeric_is_valid(numeric){
	if (numeric.attr("data-compulsory")=="no" && value_is_empty(numeric.val())){
		return true;
	}
	else{
		var re = /^[0-9]+$/;
		return re.test(numeric.val());
	}
}

function text_is_valid(text){
	if (text.attr("data-compulsory")=="no"){
		return true;
	}
	else if (value_is_empty(text.val())){
		return false;
	}
	else{
		return true;
	}
}

function select_is_valid(select){
	if (select.attr("data-compulsory")=="no"){
		return true;
	}
	else {
		return select.val() != select.attr("data-select-default");
	}
}

function wait4finalize(api_url, query, attempts){
	if (attempts < 1){
		save_data();
		$("#finalizing_time").modal('hide');
		$("#error_finalizing_time").modal('show');
	}
	var timestamp = new Date();
	$.get( api_url, { query: query, timestamp: timestamp.getTime()}).always(function(data){
		try{
			var response = $.parseJSON(data);
			if (response.status == "done" && response.data=="true"){
		  		$("#finalizing_time").modal('hide');
				$('form:first').submit();
			}
			else if (response.status == "waiting"){
				setTimeout(function(){
					wait4finalize(api_url, query, attempts-1);
				}, 5000);
			}
			else{
				save_data();
				$("#finalizing_time").modal('hide');
				$("#error_finalizing_time").modal('show');
			}
		}
		catch (err){
			save_data();
			$("#finalizing_timer").modal('hide');
			$("#error_finalizing_time").modal('show');
		}
	});
}

function save_data(){
	var form = $( "form:first" );
	$.post(form.attr('action')+"&ajax_flag=1", form.serialize()).always(
		function(data){
			//alert(data);
		}
	);
}

function submit_form(){
	if($('#client_type_1').is(':checked')) {
		$('.agent_details_container').find('[name]').each(function(event) {
			var name = $(this).attr('name');
			$('.vendor_details_container').find('[name="'+name+'"]').remove();
		});
	} else {
		$('.vendor_details_container').find('[name]').each(function(event) {
			var name = $(this).attr('name');
			$('.agent_details_container').find('[name="'+name+'"]').remove();
		});
	}
	save_data();
	if (!do_finalize){
		$('form:first').submit();
		return;
	}
	$("#finalizing_time").modal('show');
	if(window.location.href.substr(0,4) == "file"){
		setTimeout(function (){
			$("#finalizing_time").modal('hide');
			window.location.href="step_7.html";
		}, 5000);
		return;
	}
	var timestamp = new Date();
	$.get( api_url, { query: request_finalize_query, timestamp: timestamp.getTime()}).always(function(data) {
		var result = false;
		try{
			var response = $.parseJSON(data);
			result = response.result;
		}
		catch(err){
			result = false;
		}
		if (result){
			setTimeout(function (){
				wait4finalize(api_url, response_finalize_query, 10);
			}, 5000);					
		}
		else{
			save_data()
			$("#finalizing_time").modal('hide');
			$("#error_finalizing_time").modal('show');
		}
		
	});
}

function change_tab(origin_id, destination_id){
	if (destination_id <= origin_id){
		$('#accordion').accordion({active: destination_id});
	}
	else if (origin_id != 0 && destination_id-origin_id>1){
		return;
	}
	else{
		validate_all_fields();
		if ($("#ui-accordion-accordion-panel-"+origin_id).find(".invalid").length){
			return;		
		}
		else{
			$('#accordion').accordion({active: destination_id});
		}
	}
	$('.invalid').removeClass('invalid');
	if (destination_id == 0){
		$('.agent_details').hide();
		$('.vendor_details').hide();
	}
	else if (destination_id < 4){
		$('.agent_details').show();
		$('.vendor_details').hide();
	}
	else{
		$('.agent_details').hide();
		$('.vendor_details').show();
	}
}
