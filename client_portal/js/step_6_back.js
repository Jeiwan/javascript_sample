$(document).ready(function() {

	var AGENT = 1;
	var VENDOR = 2;

	var validateEmail = function(email) {
		var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
		return re.test(email);
	};

	$('input[name="agent_email"], input[name="vendor_email"]').keyup(function(event) {
		if($(this).val() && validateEmail($(this).val())) {
			$(this).removeClass('error');
		} else {
			$(this).addClass('error');
		}
	});
	
	$('input[name="agent_email"], input[name="vendor_email"]').blur(function(event) {
		if($(this).val() && validateEmail($(this).val())) {
			$(this).removeClass('error');
		} else {
			$(this).addClass('error');
		}
	});
	
	function wait4finalize(api_url, query, attempts){
		if (attempts < 1){
			save_data();
			$("#finalizing_time").modal('hide');
			$("#error_finalizing_time").modal('show');
		}
		var timestamp = new Date();
		$.get( api_url, { query: query, timestamp: timestamp.getTime()}).always(function(data){
			try{
				var response = $.parseJSON(data);
				if (response.status == "done" && response.data=="true"){
			  		$("#finalizing_time").modal('hide');
					$('form:first').submit();
				}
				else if (response.status == "waiting"){
					setTimeout(function(){
						wait4finalize(api_url, query, attempts-1);
					}, 5000);
				}
				else{
					save_data();
					$("#finalizing_time").modal('hide');
					$("#error_finalizing_time").modal('show');
				}
			}
			catch (err){
				save_data();
				$("#finalizing_timer").modal('hide');
				$("#error_finalizing_time").modal('show');
			}
		});
	}
	
	function save_data(){
		var form = $( "form:first" );
		$.post(form.attr('action'), form.serialize());
	}
	
	function submit_form(){
		if (!do_finalize){
			$('form:first').submit();
			return;
		}
		$("#finalizing_time").modal('show');
		if(window.location.href.substr(0,4) == "file"){
			setTimeout(function (){
				$("#finalizing_time").modal('hide');
				window.location.href="step_7.html";
			}, 5000);
			return;
		}
		var timestamp = new Date();
		$.get( api_url, { query: request_finalize_query, timestamp: timestamp.getTime()}).always(function(data) {
			var result = false;
			try{
				var response = $.parseJSON(data);
				result = response.result;
			}
			catch(err){
				result = false;
			}
			if (result){
				setTimeout(function (){
					wait4finalize(api_url, response_finalize_query, 10);
				}, 5000);					
			}
			else{
				save_data()
				$("#finalizing_time").modal('hide');
				$("#error_finalizing_time").modal('show');
			}
			
		});
	}

	$('.save-details-btn').click(function(event) {
		event.preventDefault();
		
		var valid = true;
		var lot_no = $('input[name="lot_number"]').val();
		$('input[name="lot_number"]').removeClass('invalid');
		if((lot_no == undefined) || (lot_no == '')) {
			valid = false;
			$('input[name="lot_number"]').toggleClass('invalid');
			$('input[name="lot_number"]').focus();
			return;
		}
		var parentClass = '';
		var selected = $('input[name="client_type"]:checked').val();
		if($('#client_type_1').is(':checked')) {
			parentClass = '.agent_details';
		} else if ($('#client_type_2').is(':checked')) {
			parentClass = '.vendor_details';
		}

		$(parentClass).next().find('input[type=email]').each(function() {
			if($(this).val() && validateEmail($(this).val())) {
				$(this).removeClass('invalid');
			} else {
				$(this).addClass('invalid');
				valid = false;
			}
		});

		if(valid) {
			if(isDataValid()) {
				var selected = $('input[name="client_type"]:checked').val();
				var propDetsForm = $('form:first');
				if(selected == AGENT) {
					propDetsForm.find('.vendor_details').next().remove();
				} else if (selected == VENDOR) {
					propDetsForm.find('.agent_details').next().remove();
				}

				submit_form();
			} else {
				//alert("Some required data was not provided. Please go through all the tabs to ensure data is entered correctly");
			}
		} else {
			//alert("At least one, of the provided email addresses, failed");
		}
	});

	$('.phone_input').blur(function(event) {
		$(this).removeClass('error');
		var target = event.target;
		var pattern = new RegExp('^[0-9\(][^A-Za-z]+[0-9\)]$');
		if(!pattern.test(target.value)) {
			$(this).addClass('error');
		}
	});

	$('input[name="agent_first_name"],input[name="agent_last_name"],input[name="vendor_first_name"],input[name="vendor_last_name"]').keydown(function(event) {
		var kc = 0;

		if(window.event) {
			kc = event.keyCode;
		} else if(event.which) {
			kc = event.which;
		}


		if( (kc>=48 && kc<=57) || (kc>=96 && kc<=105)) {
			return false;
		} else {
			var content = $(this).val();
			if(content.length < 50) {
				return true;
			} else {
				if( (kc==8) || (kc==9) || (kc==46) || (kc>=35 && kc <=40) ) {
					return true;
				} else {
					return false;
				}
			}
		}
	});

	$('input[name="agent_first_name"],input[name="agent_last_name"],input[name="vendor_first_name"],input[name="vendor_last_name"]').blur(function(event) {
		$(this).removeClass('error');
		var target = event.target;
		var pattern = new RegExp('^[A-Za-z ]+[A-Za-z]$');
		if(!pattern.test(target.value)) {
			$(this).addClass('error');
		}
	});

	$('input[name="address_1"],input[name="address_2"],input[name="agent_address"],input[name="vendor_address"],input[name="sale_address"]').keydown(function(event) {
		var content = $(this).val();
		var kc = 0;

		if(window.event) {
			kc = event.keyCode;
		} else if(event.which) {
			kc = event.which;
		}

		if(content.length < 150) {
			return true;
		} else {
			if( (kc==8) || (kc==9) || (kc==46) || (kc>=35 && kc <=40) ) {
				return true;
			} else {
				return false;
			}
		}
	});
	$('input[name="agency"],input[name="branch"]').keydown(function(event) {
		var content = $(this).val();
		var kc = 0;

		if(window.event) {
			kc = event.keyCode;
		} else if(event.which) {
			kc = event.which;
		}

		if(content.length < 50) {
			return true;
		} else {
			if( (kc==8) || (kc==9) || (kc==46) || (kc>=35 && kc <=40) ) {
				return true;
			} else {
				return false;
			}
		}
	});
	
	$('input[name="lot_number"]').blur(function (){
		if ($(this).val() == ''){
			$(this).removeClass('error');
			return;
		}
		if (!$(this).val().match(/^\d+$/)){
			$(this).addClass('error');
		}
		else{
			$(this).removeClass('error');
		}
		
	});

	// HACK to hide details until needed
	$('.agent_details').hide();
	$('.vendor_details').hide();
	// END HACK
	/* step 6 */
	$(function() {
		$("#accordion").accordion({
			heightStyle: "content",
            autoHeight: false,
        	clearStyle: true,
        	event: "none" 
		});
		$("#accordion h3").click(function (event){
			var active_id = $( "#accordion" ).accordion( "option", "active" );
			var res = $(this).attr('id').split('-');
			var destination_id = parseInt(res[res.length -1]);
			change_tab(active_id, destination_id);
		});
	});
	/*
	$('#client_type_1').click(function() {
   		if($('#client_type_1').is(':checked')) {
			$(".agent_details").show();
			$(".vendor_details").hide();
			$('#accordion').accordion({active: 1});
		} else {
			$(".agent_details").hide();
			$(".vendor_details").show();
		}
	});
	$('#client_type_2').click(function() {
   		if($('#client_type_2').is(':checked')) {
			$(".agent_details").hide();
			$(".vendor_details").show();
			$('#accordion').accordion({active: 1});
		} else {
			$(".agent_details").show();
			$(".vendor_details").hide();
		}
	});
	*/
	/* end step 6 */

	$('input[name="client_type"]:checked').each(function() {
   		if($(this) == $('#client_type_1')) {
			$(".agent_details").show();
			$(".vendor_details").hide();
		} else {
			$(".agent_details").hide();
			$(".vendor_details").show();
		}

		if($('#client_type_2').is(':checked')) {
			$(".agent_details").hide();
			$(".vendor_details").show();
			
		} else {
			$(".agent_details").show();
			$(".vendor_details").hide();
		}
	});

	// ========= Code to copy input data from element to element ======
	$('.agent_details_container input').blur(function(event) {
		var name = $(this).attr('name');
		$('.vendor_details_container input[name="'+name+'"]').val($(this).val());
	});

	$('.vendor_details_container input').blur(function(event) {
		var name = $(this).attr('name');
		$('.agent_details_container input[name="'+name+'"]').val($(this).val());
	});
	// ========= END: Code to copy input data from element to element ======

	$('#next_btn').click(function(event) {
		event.preventDefault();
		var destination_id = 0;
		if($('#client_type_1').is(':checked')) {
			$(".agent_details").show();
			$(".vendor_details").hide();
			destination_id = 1;
		} else {
			$(".agent_details").hide();
			$(".vendor_details").show();
			destination_id = 4;
		}
		change_tab(0, destination_id);
	});
	
	$('.nav-btn').click(function(event){
		event.preventDefault();
		var origin_id = parseInt($(this).attr('data-origine'));
		var destination_id = parseInt($(this).attr('data-destination'));
		change_tab(origin_id, destination_id);
	});
	
	$("select[name='sale_type']").click(
		function (){
			if ($(this).val() == 'Auction'){
				$("input[name='sale_date']").val("");
				$(".sale-div").show();	
			}
			else{
				$(".sale-div").hide();
				$("input[name='sale_date']").val("2099-01-01");
			}
		}
	);
	
	$("select[name='sale_type']").click();

});

function isDataValid() {
	var AGENT = 1;
	var VENDOR = 2;

	var valid = true;
	var selected = $('input[name="client_type"]:checked').val();
	$('form:first input').removeClass('error');
	if($('#client_type_1').is(':checked')) {
		valid = fnCheckClassElements('.agent_details');
	} else if ($('#client_type_2').is(':checked')) {
		valid = fnCheckClassElements('.vendor_details');
	}
	else{
		valid = false;
	}

	// Check property details
	if(($('form:first input[name="address_1"]').val() == undefined)
		|| ($('form:first input[name="address_1"]').val() == null)
		|| ($('form:first input[name="address_1"]').val() == '')) {
			$('form:first input[name="address_1"]').addClass('error');
			valid = false;
	}
	
	if (!$("input[name='lot_number']").val().match(/^\d+$/)){
		$("input[name='lot_number']").addClass('error');
		valid = false;
	}
	return valid;
}

function fnCheckClassElements(classToCheck) {
	var valid = true;
	var detailsDiv = $('form:first').find(classToCheck).next();

	// Check vendor details
	if((detailsDiv.find('input[name="vendor_first_name"]').val() == undefined)
		|| (detailsDiv.find('input[name="vendor_first_name"]').val() == null)
		|| (detailsDiv.find('input[name="vendor_first_name"]').val() == '')) {
			detailsDiv.find('input[name="vendor_first_name"]').addClass('error');
			valid = false;
	}

	if((detailsDiv.find('input[name="vendor_last_name"]').val() == undefined)
		|| (detailsDiv.find('input[name="vendor_last_name"]').val() == null)
		|| (detailsDiv.find('input[name="vendor_last_name"]').val() == '')) {
			detailsDiv.find('input[name="vendor_last_name"]').addClass('error');
			valid = false;
	}

	if((detailsDiv.find('input[name="vendor_address"]').val() == undefined)
		|| (detailsDiv.find('input[name="vendor_address"]').val() == null)
		|| (detailsDiv.find('input[name="vendor_address"]').val() == '')) {
			detailsDiv.find('input[name="vendor_address"]').addClass('error');
			valid = false;
	}

	if((detailsDiv.find('input[name="vendor_phone"]').val() == undefined)
		|| (detailsDiv.find('input[name="vendor_phone"]').val() == null)
		|| (detailsDiv.find('input[name="vendor_phone"]').val() == '')) {
			detailsDiv.find('input[name="vendor_phone"]').addClass('error');
			valid = false;
	}

	// Check agent details
	if((detailsDiv.find('input[name="agent_first_name"]').val() == undefined)
		|| (detailsDiv.find('input[name="agent_first_name"]').val() == null)
		|| (detailsDiv.find('input[name="agent_first_name"]').val() == '')) {
			detailsDiv.find('input[name="agent_first_name"]').addClass('error');
			valid = false;
	}

	if((detailsDiv.find('input[name="agent_last_name"]').val() == undefined)
		|| (detailsDiv.find('input[name="agent_last_name"]').val() == null)
		|| (detailsDiv.find('input[name="agent_last_name"]').val() == '')) {
			detailsDiv.find('input[name="agent_last_name"]').addClass('error');
			valid = false;
	}

	if((detailsDiv.find('input[name="agent_address"]').val() == undefined)
		|| (detailsDiv.find('input[name="agent_address"]').val() == null)
		|| (detailsDiv.find('input[name="agent_address"]').val() == '')) {
			detailsDiv.find('input[name="agent_address"]').addClass('error');
			valid = false;
	}

	if((detailsDiv.find('input[name="agent_phone"]').val() == undefined)
		|| (detailsDiv.find('input[name="agent_phone"]').val() == null)
		|| (detailsDiv.find('input[name="agent_phone"]').val() == '')) {
			detailsDiv.find('input[name="agent_phone"]').addClass('error');
			valid = false;
	}

	// Check Sale details
	if((detailsDiv.find('input[name="sale_date"]').val() == undefined)
		|| (detailsDiv.find('input[name="sale_date"]').val() == null)
		|| (detailsDiv.find('input[name="sale_date"]').val() == '')) {
			detailsDiv.find('input[name="sale_date"]').addClass('error');
			valid = false;
	}
	/*
	if((detailsDiv.find('textarea[name="sale_details"]').val() == undefined)
		|| (detailsDiv.find('textarea[name="sale_details"]').val() == null)
		|| (detailsDiv.find('textarea[name="sale_details"]').val() == '')) {
			detailsDiv.find('textarea[name="sale_details"]').addClass('error');
			valid = false;
	}
	*/

	/*if((detailsDiv.find('select[name="sale_type"]').val() == undefined)
		|| (detailsDiv.find('select[name="sale_type"]').val() == null)
		|| (detailsDiv.find('select[name="sale_type"]').val() == '')) {
			detailsDiv.find('select[name="sale_type"]').addClass('error');
			valid = false;
	}*/
	return valid;
}

function change_tab(origin_id, destination_id){
	if (destination_id == 0){
		$('.agent_details').hide();
		$('.vendor_details').hide();
	}
	if (destination_id <= origin_id){
		$('#accordion').accordion({active: destination_id});
		return;
	}
	if (origin_id != 0 && destination_id-origin_id>1){
		return;
	}
	isDataValid();
	if ($("#ui-accordion-accordion-panel-"+origin_id).find(".error").length){
				
	}
	else{
		$(".error").removeClass("error");
		$('#accordion').accordion({active: destination_id});
	}
}
