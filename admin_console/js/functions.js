$(document).ready(function() {
	/* step 1 */
	$("#forgot_password").click(function(){
  		$("#password_field").hide();
		$(this).hide();
		$("#login_btn").replaceWith("<a id='login_btn' href='step_2.html' class='btn btn-default pull-right'>Reset password</a>");
	});
	/* end step 1 */
	
	/* step 2 */
	$('#tabbed a').click(function (e) {
  		e.preventDefault()
  		$(this).tab('show')
	})
	
	$(function() {
		$("#accordion").accordion();
	});
	
	$("#view_past_prices").click(function(){
		$("#bp_current_prices").hide();
		$("#bp_past_prices").show();
		$("#bp_past_prices)detail").hide();
	});
	
	$("#view_current_prices").click(function(){
		$("#bp_current_prices").show();
		$("#bp_past_prices").hide();
		$("#bp_past_prices)detail").hide();
	});
	
	$(".view_past_prices_detail").click(function(){
		$("#bp_current_prices").hide();
		$("#bp_past_prices").hide();
		$("#bp_past_prices_detail").show();
	});
	
	$("#back_to_past_prices").click(function(){
		$("#bp_current_prices").hide();
		$("#bp_past_prices").show();
		$("#bp_past_prices_detail").hide();
	});
	
	$("#back_to_current_prices").click(function(){
		$("#bp_current_prices").show();
		$("#bp_past_prices").hide();
		$("#bp_past_prices_detail").hide();
	});
	
	$(document).on("click", function () {
		$("#inspector_type").hide();
		$("#postcode_dropdown").hide();
		$('.inspector_actions_edit').hide();
	});
	
	/* custom dropdowns */
	$('#inspector_name').keydown(function(e) {
		var code = e.keyCode || e.which;
		if (code == '9') {
			$("label[for='inspector_type']").click();
			return false;
		}
		return true;
	});
	
	$('#inspector_type').keydown(function(e) {
		var code = e.keyCode || e.which;
		if (code == '9') {
			$("label[for='inspector_postcode']").click();
			return false;
		}
		return true;
	});
	
	$('#inspector_postcode').keydown(function(e) {
		var code = e.keyCode || e.which;
		if (code == '9') {
			$('#postcode_dropdown').toggle();
		}
		return true;
	});
	
	$("label[for='inspector_type']").click(function(event){
		event.stopPropagation();
		$('#inspector_type').toggle();
		$('#postcode_dropdown').hide();
    });
	$("label[for='inspector_postcode']").click(function(event){
		event.stopPropagation();
		$('#postcode_dropdown').toggle();
		$('#inspector_type').hide();
    });
	$(".custom_select").on("click", function (event) {
		event.stopPropagation();
	});
	$("#postcode_dropdown").on("click", function (event) {
		event.stopPropagation();
	});
	$("#inspector_type").change(function() {
		var selected_values = "";
    	$("#inspector_type option:selected").each(function() {
      		selected_values += $(this).text() + ", ";
    	});
    	$("label[for='inspector_type']").text(selected_values);
  	})
	$("#inspector_postcode").change(function() {
		var selected_values = "";
    	$("#inspector_postcode option:selected").each(function() {
      		selected_values += $(this).text() + ", ";
    	});
    	$("label[for='inspector_postcode']").text(selected_values);
  	})
  	.trigger("change");


	jQuery.fn.filterByText = function(textbox, selectSingleMatch) {
        return this.each(function() {
            var select = this;
            var options = [];
            $(select).find('option').each(function() {
                options.push({value: $(this).val(), text: $(this).text()});
            });
            $(select).data('options', options);
            $(textbox).bind('change keyup', function() {
                var options = $(select).empty().data('options');
                var search = $(this).val().trim();
                var regex = new RegExp(search,"gi");
              
                $.each(options, function(i) {
                    var option = options[i];
                    if(option.text.match(regex) !== null) {
                        $(select).append(
                           $('<option>').text(option.text).val(option.value)
                        );
                    }
                });
                if (selectSingleMatch === true && $(select).children().length === 1) {
                    $(select).children().get(0).selected = true;
                }
            });            
        });
    };

    $(function() {
        $('#inspector_postcode').filterByText($('#inspector_postcode_search'), true);
    });


	/* inspector actions */
	
	$('#inspectors').on('click', '.inspector_edit', function(){
		event.stopPropagation();
		$(this).next('.inspector_actions_edit').toggle();
    });	
	$(".inspector_actions_edit").on("click", function (event){
		event.stopPropagation();
	});
	
	$(".delete_inspector").click(function(){
		$(this).closest('tr').addClass('deleted');
    });
	
	$(".undo_delete_inspector").click(function(){
		$(this).closest('tr').removeClass('deleted');
	});
	
	$('#inspectors').on('click', '.delete_inspector', function(){
		$(this).closest('tr').addClass('deleted');
	});
	
	$('#inspectors').on('click', '.undo_delete_inspector', function(){
		$(this).closest('tr').removeClass('deleted');
	});
	
	/* bp prices table */
	
	$("#bp_add_row").click(function(event){
		event.preventDefault();
		var price_name = $("#price_name").val();
		var cost_name = $("#cost_name").val();
		
		/* * * BUILD REQUEST STRING * * */
		bp_request_string += "price_name[]="+price_name+"&";
		bp_request_string += "cost_name[]="+cost_name+"&";

		$('#bp_current_prices tr:last').after("<tr><td>" + price_name + "</td><td>" + "$" + cost_name + "</td><td><a href='#_' class='btn btn-default bp_remove_row'><span class='glyphicon glyphicon-remove'></span></a></td></tr>");
		
		/* Clear input fields */
		$("#price_name").val('');
		$("#cost_name").val('');
		$('#no_unpublished').hide();
		$('#publish_bp_prices').show();
    });
	
	$('#bp_current_prices').on('click', '.bp_remove_row', function(){
		$(this).closest('tr').remove();
	});
	
	$("#publish_bp_prices").click(function(){
		var action_url = $('#bp_prices_form').attr('action');
		bp_request_string = bp_request_string.slice(0, bp_request_string.length-1);
		$(this).hide();
		$.post(action_url, bp_request_string).always(function (){
			$('#no_unpublished').show();
		});
		
    });
	
	/* end step 2 */
	
	/* strata prices table */
	$('#strata_prices_form button[type="submit"]').click(function(event) {
		event.stopPropagation();
		fnAddPriceToList();
		
		var newForm = $('#strata_prices_form').clone();
		newForm.append($('<input type="text" name="ajax_flag" value="1"/>'));
		$.post(newForm.attr('action'), newForm.serialize());
		newForm.remove();
		
		/* Clear form fields */
		$('#price_set').val('');
		return false;
	});
	
});

function fnAddPriceToList() {
	var firstRow = $('.past_prices table tbody tr:first');
	var newRow = firstRow.clone();
	newRow.find('td p').html("New Published Price: <strong>$"+$('#price_set').val()+"</strong>");
	newRow.insertBefore('.past_prices table tbody tr:first');
}

/* Request string for bp_prices */
bp_request_string = 'ajax_flag=1&';
