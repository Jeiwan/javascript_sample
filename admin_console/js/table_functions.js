/* inspector table functions */
$(document).ready(function() {
	$('.custom_tables').dataTable( {
		"bPaginate": true,
		"bSort": false,
		"bLengthChange": false,
		"bFilter": false,
		"iDisplayLength": 5,
		"sPaginationType": "full_numbers",
	});
	
	$('#inspector_list').delegate(".calendar_btn", "click", displayCalendar);
});

function saveInspector(divId) {
		var newForm = $('#inspector_list thead form').clone();
		newForm.append($('#inspector_name').clone().val($('#inspector_name').val()));
		newForm.append($('#inspector_type').clone().val($('#inspector_type').val()));
		newForm.append($('#inspector_postcode').clone().val($('#inspector_postcode').val()));
		newForm.append($('<input type="text" name="ajax_flag" value="1"/>'));
		$.post(newForm.attr('action'), newForm.serialize()).
							done(function(data) {
								if (data == "error"){
									alert("error");
								}
								else{
									$("#"+divId).find(".inspector_id").val(data);
								}
							})
							.fail(function() {
							alert( "error" );
							});
		newForm.remove();
		clearFormFields();
	}

function clearFormFields() {
	$('#inspector_name').val('');
	$('#inspector_type :selected').removeAttr('selected');
	$('#inspector_postcode :selected').removeAttr('selected');
	$('.inspector_type label[for="inspector_type"]').text(' ');
	$('.inspector_postcode label[for="inspector_postcode"]').text(' ');
}
			
function fnClickAddRow() {
	inspector_type_string = '';
	inspector_code_string = '';
	$("#inspector_type :selected").each(function() {
		inspector_type_string += $(this).text() + ",";
	});
	
	$("#inspector_postcode :selected").each(function() {
		inspector_code_string += $(this).text() + ",";
	});
		
	var inspector_name = $("#inspector_name").val();
	var inspector_type = inspector_type_string.slice(0, inspector_type_string.length-1);
	var inspector_postcode = inspector_code_string.slice(0, inspector_code_string.length-1);

	if (inspector_name == "" || inspector_type == "" || inspector_postcode==""){
		alert("Incomplete form data");
		return;
	}
	
	var rowId = $('#inspector_list').dataTable().fnSettings().fnRecordsTotal();	
	$('#inspector_list').dataTable().fnAddData( [
		inspector_name,
		inspector_type,
		inspector_postcode,
		"<div id='inspector_actions_"+rowId+"' class='inspector_actions_active'><input class='inspector_id' type='hidden' name='inspector_id' /> <a href='#' class='btn btn-default inspector_edit'><span class='glyphicon glyphicon-cog'></span></a><div class='inspector_actions_edit'><a href='#' class='btn btn-default'>Edit</a><a href='#' class='btn btn-default delete_inspector'>Delete</a></div><a href='#' class='calendar_btn btn btn-default'><span class='glyphicon glyphicon-calendar'></span></a></div><div class='inspector_actions_deleted'><a href='#' class='btn btn-default undo_delete_inspector'>Undo</a></div>"
		] );
	saveInspector('inspector_actions_'+rowId);	
}

function displayCalendar(){
	var inspectorId = $(this).parent().find(".inspector_id").val();
	var url = window.location.href;
	url = url.substr(0,url.lastIndexOf("/")+1) +calendar_link+'&inspector_id='+inspectorId;
	//alert(url);
	
	
	window.open(url,(new Date()).getTime(),'width=700,height=500,toolbar=0,menubar=0,location=0,status=1,scrollbars=1,resizable=1,left=0,top=0');
}
