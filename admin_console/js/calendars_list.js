$(document).ready(function() {
	$('#loading_div').show();
	$('#error_loading_div').hide();
	$('#set_calendar_form').hide();
	$('#save_invalid_div').hide();
	$('#save_loading_div').hide();
	$('#save_success_div').hide();
	$('#save_error_div').hide();
	if (window.location.href.indexOf('file://') == 0){
		$('#submit_btn').click(local_save_calendar_id);
		if (Math.random() < 0.5){
			setTimeout( "$('#loading_div').hide(); "+
						"$('#error_loading_div').hide(); "+
						"$('#set_calendar_form').show(); ", 5000);
		}
		else{
			setTimeout( "$('#loading_div').hide(); "+
						"$('#error_loading_div').show(); "+
						"$('#set_calendar_form').hide(); ", 5000);
		}
	}
	else{
		$('#submit_btn').click(save_calendar_id);
		get_calendars_list();
	}
});

function save_calendar_id(){
	$('#save_invalid_div').hide();
	$('#save_loading_div').show();
	$('#save_success_div').hide();
	$('#save_error_div').hide();
	var selected_calendar = $("input:radio[name=calendar_id]:checked").val();
	if (typeof selected_calendar === "undefined"){
		$('#save_invalid_div').show();
		$('#save_loading_div').hide();
		return;
	}
	$.post($("#set_calendar_form").attr('action'), $("#set_calendar_form").serialize()).
		done(function(data){
			$('#save_loading_div').hide();
			var result = false;
			try{
				var response = $.parseJSON(data);
				result = response.result;
			}
			catch(e){
				result = false;
			}
			if (result){
				$('#save_success_div').show();
				$('#save_error_div').hide();
			}
			else{
				$('#save_success_div').hide();
				$('#save_error_div').show();
			}
		}).
		error(function (){
			$('#save_invalid_div').hide();
			$('#save_loading_div').hide();
			$('#save_success_div').hide();
			$('#save_error_div').show();
		});
}

function get_calendars_list(){
	$.post($("#get_calendars_form").attr('action'), $("#get_calendars_form").serialize()).
		done(function(data){
			var result = false;
			try {
			   var response = $.parseJSON(data);
			   result = response.result;
			} catch(e) { 
				result = false;
			}
			
			if (result){
				prepare_calendars_list(response.calendars);
				$('#loading_div').hide();
				$('#error_loading_div').hide();
				$('#set_calendar_form').show();	
			}
			else{
				$('#loading_div').hide();
				$('#error_loading_div').show();
				$('#set_calendar_form').hide();
			}
		}).
		error(function (){
			$('#loading_div').hide();
			$('#error_loading_div').show();
			$('#set_calendar_form').hide();
		});
}

function prepare_calendars_list(calendars){
	$("#calendars").html("");
	var label = "";
	calendars.forEach(function (element, index, array){
		label = "<label class='radio'>";
		label += "<input type='radio' name='calendar_id' value='"+element.calendar_id+"' ";
		if (element.checked){
			label+= "checked='true' ";
		}
		label += "/>";
		label += "<span class='calendar_summary'>"+element.summary+"</span>";
		label += "</label>";
		$("#calendars").append(label);
	});
}

function local_save_calendar_id(){
	$('#save_invalid_div').hide();
	$('#save_loading_div').show();
	$('#save_success_div').hide();
	$('#save_error_div').hide();
	var selected_calendar = $("input:radio[name=calendar_id]:checked").val();
	if (typeof selected_calendar === "undefined"){
		$('#save_invalid_div').show();
		$('#save_loading_div').hide();
		return;
	}
	if (selected_calendar == "1"){
		setTimeout( "$('#save_invalid_div').hide(); "+
					"$('#save_loading_div').hide(); "+
					"$('#save_success_div').hide(); "+
					"$('#save_error_div').show();", 5000);
	}
	else{
		setTimeout( "$('#save_invalid_div').hide(); "+
					"$('#save_loading_div').hide(); "+
					"$('#save_success_div').show(); "+
					"$('#save_error_div').hide();", 5000);
	}
}

